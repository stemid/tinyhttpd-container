#!/usr/bin/env bash

set -xeu

author='Stefan Midjich <swehack at gmail dot com>'
busyboxVersion=1.36.1

builder=$(buildah from docker.io/alpine:3.18)
buildah run "$builder" -- \
  apk add --no-cache gcc musl-dev make perl curl build-base git

# Build busybox httpd
buildah run "$builder" -- \
  curl -sLO "https://busybox.net/downloads/busybox-$busyboxVersion.tar.bz2"
buildah run "$builder" -- tar -xaf "busybox-${busyboxVersion}.tar.bz2"
buildah run "$builder" -- mv "busybox-$busyboxVersion" /busybox

buildah config --workingdir /busybox "$builder"

buildah copy "$builder" busybox.config .config
buildah run "$builder" -- make
buildah run "$builder" -- make install
buildah run "$builder" -- adduser -D -h /var/www static

# Build darkhttpd
buildah run "$builder" -- \
  git clone --depth 1 https://github.com/emikulic/darkhttpd /darkhttpd
buildah config \
  --workingdir /darkhttpd \
  --env CFLAGS=' \
    -static                                 \
    -O2                                     \
    -flto                                   \
    -D_FORTIFY_SOURCE=2                     \
    -fstack-clash-protection                \
    -fstack-protector-strong                \
    -pipe                                   \
    -Wall                                   \
    -Werror=format-security                 \
    -Werror=implicit-function-declaration   \
    -Wl,-z,defs                             \
    -Wl,-z,now                              \
    -Wl,-z,relro                            \
    -Wl,-z,noexecstack                      \
  ' \
  "$builder"
buildah run "$builder" -- make darkhttpd-static
buildah run "$builder" -- strip darkhttpd-static
buildah run "$builder" -- touch /etc/httpd.conf

# Package busybox
busybox=$(buildah from scratch)

buildah copy --from "$builder" --chown 0:0 \
  "$busybox" /etc/passwd /etc/passwd
buildah copy --from "$builder" \
  "$busybox" /etc/httpd.conf /etc/httpd.conf
buildah copy --from "$builder" --chown 0:0 \
  "$busybox" /busybox/_install/bin/busybox /
buildah config \
  --workingdir /var/www \
  --port 3000 \
  --user static \
  --entrypoint '["/busybox"]' \
  --cmd '["httpd", "-f", "-v", "-p", "3000", "-c", "/etc/httpd.conf"]' \
  --author "$author" \
  "$busybox"

buildah commit "$busybox" localhost/tinyhttpd:busybox

# Package darkhttpd
darkhttpd=$(buildah from scratch)

buildah copy --from "$builder" --chown 0:0 \
  "$darkhttpd" /darkhttpd/darkhttpd-static /darkhttpd
buildah copy --from "$builder" --chown 0:0 \
  "$darkhttpd" /darkhttpd/passwd /etc/passwd
buildah copy --from "$builder" --chown 0:0 \
  "$darkhttpd" /darkhttpd/group /etc/group
buildah config \
  --workingdir /var/www \
  --port 80 \
  --entrypoint '["/darkhttpd"]' \
  --cmd '[".", "--chroot", "--uid", "nobody", "--gid", "nobody"]' \
  --author "$author" \
  "$darkhttpd"
buildah commit "$darkhttpd" localhost/tinyhttpd:darkhttpd
