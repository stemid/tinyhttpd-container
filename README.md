# Tiny httpd container image

This was meant to serve a robots.txt file with Traefik for services that don't have this feature builtin.

I built both busybox httpd and darkhttpd to see which is smaller, I kept both because they both have pro and con and I'm not sure which I prefer. Darkhttpd is much smaller though.

# Running

## darkhttpd

    podman run --rm --name darkhttpd -p 8080:80 -v "$(pwd):/var/www:Z" registry.gitlab.com/stemid/tinyhttpd-container:darkhttpd

### Pros

* Smaller container image.
* Runs in chroot.
* Simpler build process.

### Cons

* Developed by one guy.
* Build from main branch instead of tagged release.
* Shows file index by default.

## busybox httpd

    podman run --rm --name busybox -p 8080:3000 -v "$(pwd):/var/www:Z" registry.gitlab.com/stemid/tinyhttpd-container:busybox

### Pros

* Depended on by many large projects that presumably donate and ensure it survives.
* License enforced by nonprofit.
* Probably a lot more contributors.
* Build locked to stable version.
* Shows no file index by default, just 404.

### Cons

* Larger container image.
* Very tight build that does not support signals yet.
* More code = more potential bugs.
